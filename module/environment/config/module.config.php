<?php
return array(
    'controllers' => array(
        'factories' => array(
            'environment\\V1\\Rpc\\Environment\\Controller' => 'environment\\V1\\Rpc\\Environment\\EnvironmentControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'environment.rpc.environment' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/environment',
                    'defaults' => array(
                        'controller' => 'environment\\V1\\Rpc\\Environment\\Controller',
                        'action' => 'environment',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'environment.rpc.environment',
        ),
    ),
    'zf-rpc' => array(
        'environment\\V1\\Rpc\\Environment\\Controller' => array(
            'service_name' => 'environment',
            'http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PUT',
            ),
            'route_name' => 'environment.rpc.environment',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'environment\\V1\\Rpc\\Environment\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'environment\\V1\\Rpc\\Environment\\Controller' => array(
                0 => 'application/vnd.environment.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'environment\\V1\\Rpc\\Environment\\Controller' => array(
                0 => 'application/vnd.environment.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-content-validation' => array(
        'environment\\V1\\Rpc\\Environment\\Controller' => array(
            'input_filter' => 'environment\\V1\\Rpc\\Environment\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'environment\\V1\\Rpc\\Environment\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'name',
                'description' => 'name of the environment',
                'error_message' => 'please provide valid name',
            ),
        ),
    ),
);
