<?php
return array(
    'environment\\V1\\Rpc\\Environment\\Controller' => array(
        'GET' => array(
            'description' => 'get the environment data',
            'response' => '{
   "name": "se.curity"
}',
        ),
        'POST' => array(
            'description' => 'save environment data',
            'request' => '{
   "name": "name of the environment post"
}',
            'response' => '{
   "name": "name is set"
}',
        ),
        'PUT' => array(
            'request' => '{
   "name": "name of the environment in put"
}',
            'response' => '{
   "name": "name updated"
}',
            'description' => 'update environment',
        ),
        'description' => 'environment json rpc api',
    ),
);
