<?php
namespace environment\V1\Rpc\Environment;

class EnvironmentControllerFactory
{
    public function __invoke($controllers)
    {
        return new EnvironmentController();
    }
}
