<?php
namespace environment\V1\Rpc\Environment;

use Zend\Mvc\Controller\AbstractActionController;
use ZF\ContentNegotiation\ViewModel;

class EnvironmentController extends AbstractActionController
{
    public function environmentAction()
    {
        return new ViewModel(array(
                "name" => time(),
            )
        );
    }
}
